function oneThroughTwenty() {
  for (let i=1; i<=20; i+=1)
    console.log(i)  
}
oneThroughTwenty()

function evensToTwenty() {
  for (let i=2; i<=20; i+=2)
    console.log(i)
  }
evensToTwenty()

function oddsToTwenty() {
  for (let i=1; i<20; i+=2)
    console.log(i)
}
oddsToTwenty()

function multiplesOfFive() {
  for (let i=5; i<=100; i+=5)
    console.log(i)
}
multiplesOfFive()

function squareNumbers() {
  let i=1;
    while (i**2<=100) {
      console.log(i**2)
      i+=1;
  }
}
squareNumbers()

function countingBackwards() {
  for (let i=20; i>0; i-=1)
    console.log(i)  
}
countingBackwards()

function evenNumbersBackwards() {
  for (let i=20; i>0; i-=2)
    console.log(i)
}
evenNumbersBackwards()

function oddNumbersBackwards() {
  for (let i=19; i>0; i-=2)
    console.log(i)    
}
oddNumbersBackwards()

function multiplesOfFiveBackwards() {
  for (let i=100; i>0; i-=5)
    console.log(i) 
}
multiplesOfFiveBackwards()

function squareNumbersBackwards() {
  let i=10;
    while (i**2>0) {
      console.log(i**2)
      i-=1;
    }
}
squareNumbersBackwards()